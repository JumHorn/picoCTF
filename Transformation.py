# -*- coding: UTF-8 -*-


import utils  # miscellaneous for picoCTF

#
# wide char encode
# 3个字节表示一个中文(2字节)
# 编码方式 https://leetcode.com/problems/utf-8-validation/

# 问题 字符28777如何转为UTF-8编码
# 28777是中文汉字“灩”
# 转为UTF-8是，存为二进制文件的结果是
# 0xE7 0x81 0xA9
# 对应的二进制是
# 1110 0111
# 1000 0001
# 1010 1001


def transform(data):
    flag = ""
    for c in data:
        flag += (ord(c) >> 8)+chr(ord(c) & 0xFF)
    return flag


if __name__ == "__main__":
    with open('enc', 'r') as f:
        data = f.read()
    print(transform(data))
