# -*- coding: UTF-8 -*-

from pwn import *  # pylint: disable=unused-wildcard-import


def decode(data, base):
    res = b""  # result as a bytes
    words = data.split()
    for w in words:
        if w.decode().isnumeric():
            res += chr(int(w, base)).encode()
    return res


def based(ip, port):
    r = remote(ip, port)
    try:
        while True:

            line = r.recvline()
            print(line)
            if line.startswith(b"Please give the"):  # bin
                res = decode(line, 2)
                r.sendline(res)
            elif line.startswith(b"Please give me the"):  # octal
                res = decode(line, 8)
                if res == b"":  # hex
                    data = line.split()[4]
                    for i in range(0, len(data), 2):
                        res += chr(int(data[i:i+2], 16)).encode()
                r.sendline(res)
    except:
        pass


if __name__ == "__main__":
    based("jupiter.challenges.picoctf.org", 15130)
