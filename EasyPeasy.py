# -*- coding: UTF-8 -*-

from pwn import *  # pylint: disable=unused-wildcard-import

KEY_LEN = 50000


def HexDecode(flag):
    flag = flag.rstrip()
    res = ""
    for i in range(0, len(flag), 2):
        res += chr(int(flag[i: i+2], 16))
    print(res)
    return res


def XorAttack(ip, port):

    flag = "5541103a246e415e036c4c5f0e3d415a513e4a560050644859536b4f57003d4c"
    res = HexDecode(flag)

    r = remote(ip, port)
    r.sendlineafter("What data would you like to encrypt?",
                    "0"*(KEY_LEN-len(res)))

    r.sendlineafter("What data would you like to encrypt?", res)
    r.recvuntil("Here ya go!\n")
    flag = r.recvline()
    print(flag)
    HexDecode(flag)


if __name__ == "__main__":
    XorAttack("mercury.picoctf.net", 36981)
