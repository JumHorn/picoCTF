# -*- coding: UTF-8 -*-


from pwn import *  # pylint: disable=unused-wildcard-import


def attack(ip, port):
    flag = ""
    r = remote(ip, port)
    try:
        while True:
            data = r.recvline()
            if not data:
                break
            flag += chr(int(data))
    except:  # still EOFError not processed
        pass
    return flag


if __name__ == "__main__":
    flag = attack("mercury.picoctf.net", 21135)
    print(flag)
