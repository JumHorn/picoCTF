# -*- coding: UTF-8 -*-

def bytesToHexString(content, space_seperated=True):
    if space_seperated:
        return "".join(["%02X " % b for b in content])
    else:
        return "".join(["%02X" % b for b in content])


def readFromFile(file):
    with open(file, 'rb') as f:
        return f.read()


def writeToFile(content, file):
    with open(file, 'wb') as f:
        f.write(content)


def rot13(c, rotate=13):
    if c.islower():
        c = chr(((ord(c)-ord('a')+rotate) % 26)+ord('a'))
    elif c.isupper():
        c = chr(((ord(c)-ord('A')+rotate) % 26)+ord('A'))
    return c
