# -*- coding: UTF-8 -*-

def caesar(c, rotate):
    if c.islower():
        c = chr(((ord(c)-ord('a')+rotate) % 26)+ord('a'))
    elif c.isupper():
        c = chr(((ord(c)-ord('A')+rotate) % 26)+ord('A'))
    return c


if __name__ == "__main__":
    flag = "ynkooejcpdanqxeykjrbdofgkq"
    for i in range(0, 26):
        print("".join([caesar(c, i) for c in flag]))
